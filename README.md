# Melonicious

[![Gitter chat](https://badges.gitter.im/gitterHQ/gitter.png)](https://gitter.im/Melonicious/Lobby?utm_source=share-link&utm_medium=link&utm_campaign=share-link)    [![Build Status](https://travis-ci.org/AbhilashG97/Melonicious.svg?branch=master)](https://travis-ci.org/AbhilashG97/Melonicious)

An Android app that tracks the daily commits of a given set of GitHub users
